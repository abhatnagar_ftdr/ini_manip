
import configparser
import os
#reads the ini file replaces only those config['base'][key] with values "" with os.environ.get[key_CICD]
#The key_CICD is a string constructed from key by replacing '.' with '_' and all the keys are upper-case. 

def read_from_ini():
    config = configparser.ConfigParser()
    config.read('application.ini')
    for ky in config['base'].keys():
        print(f"For the key {ky} the value is: {config.get('base', ky)}") 
        print(f"echo ${config.get('base', ky)}")