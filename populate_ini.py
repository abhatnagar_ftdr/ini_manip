import configparser
import os
#populates the ini file replaces only those config['base'][key] with values "" with os.environ.get[key_CICD]
#The key_CICD is a string constructed from key by replacing '.' with '_' and all the keys are upper-case.
        
def write_to_ini():
    config = configparser.ConfigParser()
    config.read('deploy/application/configs/application.ini')
    for ky in config['base'].keys():
        if config.get('base', ky).split(";", 1).strip() =='""': #replace using the Gitlab CI/CD
            env_var = ky.replace(".", "_").upper()+"_BASE"
            config.set('base', ky, str(os.environ.get(env_var)))
    for ky in config['qa : base'].keys():
        if config.get('qa : base', ky).split(";", 1).strip() =='""': #replace using the Gitlab CI/CD
            env_var = ky.replace(".", "_").upper()+"_QA_BASE"
            config.set('qa : base', ky, str(os.environ.get(env_var)))
    for ky in config['dev : qa'].keys():
        if config.get('dev : qa', ky).split(";", 1).strip() =='""': #replace using the Gitlab CI/CD
            env_var = ky.replace(".", "_").upper() + "_DEV_QA"
            config.set('dev : qa', ky, str(os.environ.get(env_var)))
    for ky in config['production : base'].keys():
        if config.get('production : base', ky).split(";", 1).strip() =='""': #replace using the Gitlab CI/CD
            env_var = ky.replace(".", "_").upper() +"_PROD_BASE"
            config.set('production : base', ky, str(os.environ.get(env_var)))
    with open('application.ini', 'a+') as configfile:    # save
        config.write(configfile) 

if __name__ == "__main__":
    write_to_ini()