import os
import configparser
import os
#populates the ini file replaces only those config['base'][key] with values "" with os.environ.get[key_CICD]
#The key_CICD is a string constructed from key by replacing '.' with '_' and all the keys are upper-case. 

def attach_suffix(in_str):
    RUNTIME_ENV = os.environ.get('RUNTIME_ENV')
    if RUNTIME_ENV == "development":
        print(f"echo RUNTIME_ENV={RUNTIME_ENV}")
        return in_str+"_DEV"
    elif RUNTIME_ENV =="qa":
        return in_str+"_QA"
    elif RUNTIME_ENV == "prod":
        return in_str+"_PROD"
    else:
        return in_str
USER = os.getenv('API_USER')

refName = os.environ.get("CI_COMMIT_REF_NAME")
piplineID = os.environ.get("CI_PIPELINE_ID")
notification = os.environ.get("Notifications_Adapter_SMS_Account_Token")
relVersion = refName + ".0." + piplineID

version = relVersion.replace("rel.", "")
print("current version is", version)
print(f"current notification={notification}")
print("export VERSION='{}'".format(version))
print("export notification='{}'".format(notification))